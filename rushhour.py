import map
from node import Node
import time

__author__ = 'Jan Trenčanský'
expansion = 0


def all_moves(node):  # returns all valid moves for node
    state = node.state
    for i in state.cars:
        car_id = i[0]
        y = i[1]
        x = i[2]
        for action in actions:
            movement = state.is_movable(action, y, x)
            if movement[1] != 0:
                movement.append(car_id)
                movement.append(action)
                yield movement


def generate_all_child(node):  # creates every new state based on list of valid moves and makes it a child
    global expansion
    expansion += 1
    for move in all_moves(node):
        action = move[-1]
        car = move[0]
        car_id = move[-2]
        for step in range(1, move[1]+1):
            child_state = node.state.action(car_id, car, action, step)
            child_node = Node(child_state, node)
            if child_state not in visited:  # if child_state was not parent of current state, add
                visited.add(child_state)
                node.add_child(child_node)


def dls(node, node_depth):
    if node_depth >= 0:
        generate_all_child(node)  # generate children
        if node.state.is_solved():  # if solution is found return that node
            return node
        else:
            for child in node.children:
                state = dls(child, node_depth-1)
                if state is not None:  # if dls() found solution node keep returning this state to the top
                    return state


def print_moves(state):  # load all moves from final state to beginning reverse list and print
    moves = []
    while state.parent is not None:
        moves.append(state.state.move)
        state = state.parent
    print("Number of moves:" + str(len(moves)))
    for move in reversed(moves):
        print(str(move[1]) + "(" + str(move[0]) + ", " + str(move[2]) + ")")


def get_time(start, end):  # print timer for dls()
    sec = end - start
    print("Runtime:", sec, "seconds")

# List of possible actions
actions = ("left", "right", "up", "down")
#Load initial state
mapa = map.Map()
filename = input("Filename:")
mapa.load(filename)
#Load depth
depth = int(input("Depth:"))
print("Initial state:")

mapa.display_matrix()
mapa.load_cars()
#Put initial state into root
root = Node(mapa, None)
final = None

start = time.time()  # starts timer
for i in range(1, depth+1):
    visited = set()
    print("Depth:", i, "\r", end=' ')  # show current depth
    final = dls(root, i)
    if final is not None:  # then solution was found we can stop searching
        end = time.time()  # end timer
        break

print()
if final is not None:
    get_time(start, end)
    print("Solution found in depth", i, ":")
    final.state.display_matrix()
    print("Moves:")
    print_moves(final)
    print("Number of node expansion:", expansion)
else:
    print("Solution not found! Increase depth", depth)
