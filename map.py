import csv
from copy import deepcopy


class Map:
    __slots__ = ('matrix', 'cars', 'move')  # this is just performance improvement (or it should be)

    def __init__(self):
        self.matrix = [[0 for x in range(6)] for y in range(6)]  # state of board
        self.cars = []  # list of all cars and their first found coordinate
        self.move = None  # move used to generate this state

    def __eq__(self, other):  # compares two boards
        str1 = ""  # it's pretty ugly, haven't thought of anything better
        str2 = ""
        for i in range(0, 6):
            for j in range(0, 6):
                str1 += str(self.matrix[i][j])
                str2 += str(other.matrix[i][j])
        if str1 == str2:
            return 1
        else:
            return 0

    def __hash__(self):
        str1 = ""  # this is useful together with __eq__()
        for i in range(0, 6):
            for j in range(0, 6):
                str1 += str(self.matrix[i][j])
        return hash(str1)

    def load(self, filename):  # loads initial state of board from csv file(format: car_id,size,
        try:
            reader = csv.reader(open(filename, newline=""), dialect="excel")
        except IOError:
            print("Could not open file: " + filename)
            return 0
        for row in reader:
            car = int(row[0])
            size = int(row[1])
            y = int(row[2])-1
            x = int(row[3])-1
            direction = row[4]

            self.matrix[y][x] = car
            if direction == "h":  # not necessary horizontal cars are even
                for i in range(0, size):
                    self.matrix[y][x+i] = car
            elif direction == "v":  # and vertical cars are odd
                for i in range(0, size):
                    self.matrix[y+i][x] = car

    def load_cars(self):
        tmp = []
        del (self.cars[0:len(self.cars)])
        for i in range(6):
            for j in range(6):
                if (self.matrix[i][j] not in tmp) & (self.matrix[i][j] != 0):
                    tmp.append(self.matrix[i][j])
                    clist = (self.matrix[i][j], i, j)  # car_id, and first found coordinates
                    self.cars.append(clist)

    def display_matrix(self):  # prints matrix
        for row in self.matrix:
            for item in row:
                print(str(item) + "\t", end='')
            print()
        print()

    def is_movable(self, action, y, x):  # finds there are any valid movements for action
        car = []  # coordinates of car (all of them)
        car_id = self.matrix[y][x]
        # Get car coordinates
        if car_id % 2 == 0:  # horizontal car
            for i in range(6):
                if self.matrix[y][i] == car_id:
                    car.append((y, i))
        elif car_id % 2 != 0:  # vertical car
            for i in range(6):
                if self.matrix[i][x] == car_id:
                    car.append((i, x))
        counter = 0
        if (action == "left") & (car_id % 2 == 0):
            try:
                for i in range((car[0][1])-1, -1, -1):
                    if self.matrix[car[0][0]][i] == 0:
                        #  print("[" + str(car[0][0]) + "][" + str(i) + "]=", end="")  # print coordinates
                        #  print(self.matrix[car[0][0]][i])  # print value
                        counter += 1
                    elif self.matrix[car[0][0]][i] != 0:
                        break
            except IndexError:
                print("Out of range")
        elif (action == "right") & (car_id % 2 == 0):
            try:
                for i in range((car[-1][1])+1, 6):
                    if self.matrix[car[-1][0]][i] == 0:
                        counter += 1
                    elif self.matrix[car[0][0]][i] != 0:
                        break
            except IndexError:
                print("Out of range")
        elif (action == "up") & (car_id % 2 != 0):
            try:
                for i in range((car[0][0])-1, -1, -1):
                    if self.matrix[i][car[0][1]] == 0:
                        counter += 1
                    elif self.matrix[i][car[0][1]] != 0:
                        break
            except IndexError:
                print("Out of range")
        elif (action == "down") & (car_id % 2 != 0):
            try:
                for i in range((car[-1][0])+1, 6):
                    if self.matrix[i][car[0][1]] == 0:
                        counter += 1
                    elif self.matrix[i][car[0][1]] != 0:
                        break
            except IndexError:
                print("Out of range")
        output = [car, counter]
        return output

    def action(self, car_id, car, action, step):  # move car and return new state
        new_map = Map()
        new_map.matrix = deepcopy(self.matrix)  # create copy of current state
        for coordinates in car:  # delete car
            y = coordinates[0]
            x = coordinates[1]
            new_map.matrix[y][x] = 0

        if (action == "right") & (car_id % 2 == 0):
            for coordinates in car:
                y = coordinates[0]
                x = coordinates[1] + step
                new_map.matrix[y][x] = car_id
                new_map.load_cars()
                new_map.move = (car_id, action, step)
            return new_map

        elif (action == "left") & (car_id % 2 == 0):
            for coordinates in car:
                y = coordinates[0]
                x = coordinates[1] - step
                new_map.matrix[y][x] = car_id
                new_map.load_cars()
                new_map.move = (car_id, action, step)
            return new_map

        elif (action == "up") & (car_id % 2 != 0):
            for coordinates in car:
                y = coordinates[0] - step
                x = coordinates[1]
                new_map.matrix[y][x] = car_id
                new_map.load_cars()
                new_map.move = (car_id, action, step)
            return new_map

        elif (action == "down") & (car_id % 2 != 0):
            for coordinates in car:
                y = coordinates[0] + step
                x = coordinates[1]
                new_map.matrix[y][x] = car_id
                new_map.load_cars()
                new_map.move = (car_id, action, step)
            return new_map
        else:
            print("Invalid move")
            return 0

    def is_solved(self):
        for i in range(6):  # if there is car 2 (red car) in column 5 (6 if we count form 1) solution is found
            if self.matrix[i][5] == 2:  # red car must always have id 2!
                return 1
        return 0