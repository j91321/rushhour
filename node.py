class Node:
    __slots__ = ('state', 'parent', 'depth', 'children')

    def __init__(self, state, parent):
        self.state = state  # map object
        self.parent = parent
        if parent is None:  # depth of the node based on parent depth
            self.depth = 0
        elif parent is not None:
            self.depth = parent.depth + 1
        self.children = []  # list of children

    def add_child(self, child):
        self.children.append(child)